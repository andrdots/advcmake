set(CMAKE_MODULE_PATH
    "${CMAKE_CURRENT_LIST_DIR}/cmake-sanitizers/cmake"
    ${CMAKE_MODULE_PATH}
)

find_package(Sanitizers)

macro(declare_interface_library TARGET_NAME)
    add_library("${TARGET_NAME}" INTERFACE)
endmacro()

macro(declare_static_library TARGET_NAME)
    add_library("${TARGET_NAME}" STATIC "")
endmacro()

macro(declare_shared_library TARGET_NAME)
    add_library("${TARGET_NAME}" SHARED "")
endmacro()

macro(declare_executable TARGET_NAME)
    add_executable("${TARGET_NAME}")
endmacro()

macro(protect_target TARGET_NAME)
    add_sanitizers("${TARGET_NAME}")
endmacro()

